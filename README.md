# RedBird::Demo

This gem is a practical example of how to use the RedBird game engine. It is also useful to test changes into the RedBird.

## Installation

You can install this gem through the `gem` command:

    $ gem install red_bird-demo

## Usage

After installing the gem, run `red_bird-demo`.

## Development

To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/fredlinhares/red_bird-demo.

## License

Read LICENSE.txt for information about licensing.
