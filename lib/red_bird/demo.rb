# SPDX-License-Identifier: MIT
require "red_bird"
require "red_bird/demo/version"

module RedBird
  module Demo
    module Stage
      require_relative "demo/stage_main"
      require_relative "demo/stage_move_on_tiles"
      require_relative "demo/stage_resolution"
    end

    class Error < StandardError; end

    def self.run
      RedBird::Engine.debug = true
      RedBird::Engine.set_pixel_quantity(256, 240)
      RedBird::Engine.set_screen_resolution(256, 240)

      RedBird::Engine.run do |global_data|
        # Initialize global_data
        if global_data.empty? then

          global_data[:stage] = :main
          global_data[:palette] = RedBird::Palette.from_yml(
            Gem.datadir("red_bird-demo") + "/palette/palette.yml")
          global_data[:color] = {
            white: RedBird::Color.new(255, 255, 255, 255),
            black: RedBird::Color.new(0, 0, 0, 255)
          }
        end

        case global_data[:stage]
        when :main then
          Demo::Stage::Main.new(global_data)
        when :move_on_tiles then
          Demo::Stage::MoveOnTiles.new(global_data)
        when :resolution then
          Demo::Stage::Resolution.new(global_data)
        end
      end

    end
  end
end
