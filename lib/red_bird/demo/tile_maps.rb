# SPDX-License-Identifier: MIT
require "red_bird/tile_map"

require_relative "tile_sets.rb"

module RedBird::Demo
  module TileMaps
    def self.move_on_tiles(palette)
      return RedBird::TileMap.from_yml(
               Gem.datadir("red_bird-demo"), "/tile_map/move_on_tiles.yml",
               TileSets.move_on_tiles(palette), 0, 0, 256, 240)
    end
  end
end
