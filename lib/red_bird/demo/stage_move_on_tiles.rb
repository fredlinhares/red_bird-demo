# SPDX-License-Identifier: MIT
require 'red_bird/camera'
require 'red_bird/stage'

module RedBird::Demo::Stage
  require_relative 'controller_move_on_tiles'
  require_relative 'entity_move_on_tiles'
  require_relative 'tile_maps'

  class MoveOnTiles < RedBird::Stage
    def initialize(global_data)
      super(global_data)

      @tile_map = RedBird::Demo::TileMaps.move_on_tiles(
        global_data[:palette])
      RedBird::RelativeEntity.scenario = @tile_map

      @player = RedBird::Demo::Entity::MoveOnTiles.new(
        32, 64, global_data[:palette])
      @camera = RedBird::Camera.new(@player, @tile_map)

      self.add_entities([@tile_map, @player])
      @interactions = [@camera]

      @controller = RedBird::Demo::Controller::MoveOnTiles.new(@player)
      @input_device = RedBird::InputDevice.new(@controller)
    end

    def post_tick
      @interactions.each { |i| i.call }
    end
  end
end
