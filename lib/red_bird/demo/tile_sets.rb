# SPDX-License-Identifier: MIT
require "red_bird/tile_set"

module RedBird::Demo
  module TileSets
    def self.move_on_tiles(palette)
      return RedBird::TileSet.from_yml(
               Gem.datadir("red_bird-demo"), "/tile_set/move_on_tiles.yml",
               palette)
    end
  end
end
