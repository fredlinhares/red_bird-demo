# SPDX-License-Identifier: MIT
module RedBird::Demo
  module Controller
    class MoveOnTiles < RedBird::Controller
      def initialize(entity)
        super()
        @entity = entity
      end
    end
  end
end
