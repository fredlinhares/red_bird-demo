# SPDX-License-Identifier: MIT
require 'red_bird/stage'
require "red_bird/vertical_menu"

module RedBird::Demo::Stage
  class Resolution < RedBird::Stage

    def initialize(global_data)
      super(global_data)

      font = RedBird::Font.new(
        Gem.datadir("red_bird-demo") + "/font/connection.ttf", 18)

      box_style = RedBird::UIBox::Style.new(
        Gem.datadir("red_bird-demo") + "/img/text_box.pgm",
        global_data[:palette], global_data[:color][:black], 16, 16)

      resolution_menu = RedBird::VerticalMenu.new(
        box_style, 20, 20,
        [RedBird::VerticalMenu::Option.new(
           RedBird::Text.new("256x240", font, global_data[:color][:white],
                             global_data[:color][:black])
         ) { RedBird::Engine.set_screen_resolution(256, 240) },
         RedBird::VerticalMenu::Option.new(
           RedBird::Text.new("512x480", font, global_data[:color][:white],
                             global_data[:color][:black])
         ) { RedBird::Engine.set_screen_resolution(512, 480) },
         RedBird::VerticalMenu::Option.new(
           RedBird::Text.new("800x600", font, global_data[:color][:white],
                             global_data[:color][:black])
         ) { RedBird::Engine.set_screen_resolution(800, 600) },
         RedBird::VerticalMenu::Option.new(
           RedBird::Text.new("600x800", font, global_data[:color][:white],
                             global_data[:color][:black])
         ) { RedBird::Engine.set_screen_resolution(600, 800) },
         RedBird::VerticalMenu::Option.new(
           RedBird::Text.new("Back", font, global_data[:color][:white],
                             global_data[:color][:black])
         ) { global_data[:stage] = :main; RedBird::Engine::quit_stage }
        ])

      self.add_entities(resolution_menu)

      @controller = RedBird::Controller.new
      @controller.add_command(:press_up) do
        resolution_menu.pred_opt
      end
      @controller.add_command(:press_down) do
        resolution_menu.next_opt
      end
      @controller.add_command(:press_confirm) do
        resolution_menu.activate
      end

      @input_device = RedBird::InputDevice.new(@controller)
      @input_device.add_keydown(RedBird::Keycode::W, :press_up)
      @input_device.add_keydown(RedBird::Keycode::S, :press_down)
      @input_device.add_keydown(RedBird::Keycode::F, :press_confirm)
    end
  end
end
