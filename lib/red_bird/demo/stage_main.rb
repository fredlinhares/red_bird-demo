# SPDX-License-Identifier: MIT
require 'red_bird/stage'
require "red_bird/vertical_menu"

module RedBird::Demo::Stage
  class Main < RedBird::Stage

    def initialize(global_data)
      super(global_data)

      font = RedBird::Font.new(
        Gem.datadir("red_bird-demo") + "/font/connection.ttf", 18)

      box_style = RedBird::UIBox::Style.new(
        Gem.datadir("red_bird-demo") + "/img/text_box.pgm",
        global_data[:palette], global_data[:color][:black],  16, 16)

      main_menu = RedBird::VerticalMenu.new(
        box_style, 20, 20,
        [RedBird::VerticalMenu::Option.new(
           RedBird::Text.new(
             "Move on Tiles", font, global_data[:color][:white],
             global_data[:color][:black])
         ) { global_data[:stage] = :move_on_tiles
           RedBird::Engine::quit_stage },
         RedBird::VerticalMenu::Option.new(
           RedBird::Text.new(
             "Resolution", font, global_data[:color][:white],
             global_data[:color][:black])
         ) { global_data[:stage] = :resolution
           RedBird::Engine::quit_stage },
         RedBird::VerticalMenu::Option.new(
           RedBird::Text.new(
             "Exit", font, global_data[:color][:white],
             global_data[:color][:black])
         ) { RedBird::Engine::quit_game }
        ])

      self.add_entities(main_menu)

      @controller = RedBird::Controller.new
      @controller.add_command(:press_up) do
        main_menu.pred_opt
      end
      @controller.add_command(:press_down) do
        main_menu.next_opt
      end
      @controller.add_command(:press_confirm) do
        main_menu.activate
      end

      @input_device = RedBird::InputDevice.new(@controller)
      @input_device.add_keydown(RedBird::Keycode::W, :press_up)
      @input_device.add_keydown(RedBird::Keycode::S, :press_down)
      @input_device.add_keydown(RedBird::Keycode::F, :press_confirm)
    end

  end
end
