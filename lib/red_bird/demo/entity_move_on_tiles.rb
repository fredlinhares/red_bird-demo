# SPDX-License-Identifier: MIT
require 'red_bird/animation'
require 'red_bird/relative_entity'

module RedBird::Demo
  module Entity
    class MoveOnTiles < RedBird::RelativeEntity

      def initialize(relative_pos_x, relative_pos_y, palette)
        super(relative_pos_x, relative_pos_y, 32, 32)

        texture = RedBird::Texture.new(
          Gem.datadir("red_bird-demo") + "/img/player.pgm", palette)

        vertical_sprites = 1
        horizontal_sprites = 3
        @sprites = {
          walking_down: RedBird::Sprite.grid(
            texture, 0, 0,
            horizontal_sprites, vertical_sprites,
            @collision_box.width, @collision_box.height),
          walking_left: RedBird::Sprite.grid(
            texture, 0, 1 * @collision_box.height,
            horizontal_sprites, vertical_sprites,
            @collision_box.width, @collision_box.height),
          walking_right: RedBird::Sprite.grid(
            texture, 0, 2 * @collision_box.height,
            horizontal_sprites, vertical_sprites,
            @collision_box.width, @collision_box.height),
          walking_up: RedBird::Sprite.grid(
            texture, 0, 3 * @collision_box.height,
            horizontal_sprites, vertical_sprites,
            @collision_box.width, @collision_box.height)
        }

        @animations = {
          walking_down: RedBird::Animation::Loop.new(
            RedBird::Animation::Frame.sequence(
              [
                [7, @sprites[:walking_down][0]],
                [7, @sprites[:walking_down][1]],
                [7, @sprites[:walking_down][2]]
              ])),
          walking_left: RedBird::Animation::Loop.new(
            RedBird::Animation::Frame.sequence(
              [
                [7, @sprites[:walking_left][0]],
                [7, @sprites[:walking_left][1]],
                [7, @sprites[:walking_left][2]]
              ])),
          walking_right: RedBird::Animation::Loop.new(
            RedBird::Animation::Frame.sequence(
              [
                [7, @sprites[:walking_right][0]],
                [7, @sprites[:walking_right][1]],
                [7, @sprites[:walking_right][2]]
              ])),
          walking_up: RedBird::Animation::Loop.new(
            RedBird::Animation::Frame.sequence(
              [
                [7, @sprites[:walking_up][0]],
                [7, @sprites[:walking_up][1]],
                [7, @sprites[:walking_up][2]]
              ])),
        }

        @current_animation = :walking_right

        # Make entity move in circles.
        @walk_count = 0
        @number_hor_steps = 72
        @number_ver_steps = 64

        @walk_right = Proc.new do
          @collision_box.x += 4
          @walk_count += 1

          if @walk_count >= @number_hor_steps then
            @walk_count = 0
            @current_animation = :walking_down
            @walk_algorithm = @walk_down
          end
        end

        @walk_down = Proc.new do
          @collision_box.y += 4
          @walk_count += 1

          if @walk_count >= @number_ver_steps then
            @walk_count = 0
            @current_animation = :walking_left
            @walk_algorithm = @walk_left
          end
        end

        @walk_left = Proc.new do
          @collision_box.x -= 4
          @walk_count += 1

          if @walk_count >= @number_hor_steps then
            @walk_count = 0
            @current_animation = :walking_up
            @walk_algorithm = @walk_up
          end
        end

        @walk_up = Proc.new do
          @collision_box.y -= 4
          @walk_count += 1

          if @walk_count >= @number_ver_steps then
            @walk_count = 0
            @current_animation = :walking_right
            @walk_algorithm = @walk_right
          end
        end

        @walk_algorithm = @walk_right
      end

      def current_sprite
        return @animations[@current_animation].current_sprite
      end

      def tick
        @walk_algorithm.call
        @animations[@current_animation].animate
      end
    end
  end
end
